# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "H:/code/courseware/redis/src/adlist.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/adlist.c.obj"
  "H:/code/courseware/redis/src/ae.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/ae.c.obj"
  "H:/code/courseware/redis/src/anet.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/anet.c.obj"
  "H:/code/courseware/redis/src/crc64.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/crc64.c.obj"
  "H:/code/courseware/redis/src/redis-cli.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/redis-cli.c.obj"
  "H:/code/courseware/redis/src/release.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/release.c.obj"
  "H:/code/courseware/redis/src/sds.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/sds.c.obj"
  "H:/code/courseware/redis/src/zmalloc.c" "H:/code/courseware/redis/cmake-build-debug-mingw/CMakeFiles/redis-cli.dir/src/zmalloc.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../deps/linenoise"
  "../deps/hiredis"
  "../deps/lua/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "H:/code/courseware/redis/cmake-build-debug-mingw/deps/linenoise/CMakeFiles/linenoise.dir/DependInfo.cmake"
  "H:/code/courseware/redis/cmake-build-debug-mingw/deps/hiredis/CMakeFiles/hiredis.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
