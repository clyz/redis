# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "D:/Source Code/redis/src/adlist.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/adlist.c.obj"
  "D:/Source Code/redis/src/ae.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/ae.c.obj"
  "D:/Source Code/redis/src/anet.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/anet.c.obj"
  "D:/Source Code/redis/src/crc64.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/crc64.c.obj"
  "D:/Source Code/redis/src/redis-cli.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/redis-cli.c.obj"
  "D:/Source Code/redis/src/release.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/release.c.obj"
  "D:/Source Code/redis/src/sds.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/sds.c.obj"
  "D:/Source Code/redis/src/zmalloc.c" "D:/Source Code/redis/cmake-build-debug/CMakeFiles/redis-cli.dir/src/zmalloc.c.obj"
  )
set(CMAKE_C_COMPILER_ID "MSVC")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../deps/linenoise"
  "../deps/hiredis"
  "../deps/lua/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "D:/Source Code/redis/cmake-build-debug/deps/linenoise/CMakeFiles/linenoise.dir/DependInfo.cmake"
  "D:/Source Code/redis/cmake-build-debug/deps/hiredis/CMakeFiles/hiredis.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
